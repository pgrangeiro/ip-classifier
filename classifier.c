#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char* reverse(char* value) {
    int size = strlen(value);
    char* reversed = (char*)malloc(size * sizeof(char*));

    for (int i=size-1, j=0; i >= 0; i--, j++)
        reversed[j] = value[i];
    reversed[size] = '\0';

    //printf("\n %d - %d", strlen(reversed), strlen(value));
    return reversed;
}

int is_valid_ip(char* value) {
    int size = strlen(value);
    int max_delimiter = 3;
    int is_valid = 0;
    int counter = 0;

    for (int i=0; i<size; i++) {
        if (value[i] == '.')
            counter += 1;
    }

    if (counter == max_delimiter)
        is_valid = 1;

    //printf("\n is valid ip:%d", is_valid);
    return is_valid;

}

char* strip_domain(char* value) {
    int size = strlen(value);
    char* temp = (char*)malloc(size * sizeof(char));
    int max_delimiter = 3;
    int i;

    for (i=0; i<size; i++) {
        if (value[i] == '.') {
            max_delimiter -= 1;
        }
        if (max_delimiter == 0) {
            break;
        }
        temp[i] = value[i];
    }

    size = i;
    char* domain = (char*)malloc(size * sizeof(char));
    for (int j=0; j<size; j++)
        domain[j] = temp[j];
    domain[size] = '\0';

    free(temp);
    //printf("\n domain:%s", domain);
    return domain;
}

char* strip_ip(char* value) {
    int size = strlen(value) * sizeof(char*);
    char* ip = (char*)malloc(size);

    ip = reverse(value);
    ip = strtok(ip, " ");
    ip = reverse(ip);

    //printf("\n ip:%s", ip);
    return ip;
}



void print_grouped(char **data, int max_size) {

    for(int i=0; i<max_size; i++) {
        printf("%s\n", data[i]);
    }

}


char** group_ips(char** data, int max_size) {

    int found = 1, is_valid = 0, index = 0, size = 0;
    char *temp, *ip, *domain;

    int last_index = 0;
    char** result = NULL;
    result = (char**)malloc((last_index + 1) * sizeof(*result));

    char **keys = NULL;
    keys = (char**)malloc((last_index + 1) * sizeof(*keys));

    if (!!result) {
        for(int i=0; i<max_size; i++) {
            strcpy(temp, data[i]);
            ip = strip_ip(temp);
            is_valid = is_valid_ip(ip);

            if (is_valid) {

                domain = strip_domain(ip);

                if (last_index > 0) {
                    for (index=0; index<last_index; index++) {
                        found = strcmp(keys[index], domain);
                        if (found == 0)
                            break;
                    }
                }

                if (found == 0) {
                    strcpy(temp, result[index]);
                    size = (strlen(", ") + strlen(temp) + strlen(data[i])) * sizeof(char*);
                    result[index] = (char*)malloc(size);

                    if (!!result[index]) {
                        strcpy(result[index], temp);
                        strcat(result[index], ", ");
                        strcat(result[index], data[i]);
                    }
                } else {
                    result = (char**)realloc(result, (last_index + 1) * sizeof(*result));
                    keys = (char**)realloc(keys, (last_index + 1) * sizeof(*keys));

                    if (!!keys && !!result) {
                        keys[last_index] = (char*)malloc(strlen(domain) * sizeof(char));
                        strcpy(keys[last_index], domain);
                        result[last_index] = (char*)malloc(sizeof(data[i]) * sizeof(char*));
                        strcpy(result[last_index], data[i]);

                        last_index ++;
                    } else {
                        break;
                    }
                }
            }
        }
        print_grouped(result, last_index);
    }

    free(result);
    free(keys);
}


void read_file(char *file_path) {
    char** result = NULL;

    FILE* file = fopen(file_path, "r");
    if (!!file) {

        int index = 0;
        char line[255];
        int size = (index + 1) * sizeof(*result);
        result = (char**)malloc(size);

        if (!!result) {
            while (!!fgets(line, sizeof(line), file)) {

                size = (index + 1) * sizeof(*result);
                result = (char**)realloc(result, size);

                if (!!result) {
                    result[index] = (char*)malloc(strlen(line) * sizeof(char*));
                    strcpy(result[index], line);
                    result[index][strlen(result[index])-1] = '\0';
                    index ++;
                } else {
                    break;
                }
            }

            fclose(file);

            group_ips(result, index);

            free(result);
        }
    }
}


void main() {
    char file_path[14] = "sample/ips.log";
    int index = 0;

    read_file(file_path);

    exit(0);
}
